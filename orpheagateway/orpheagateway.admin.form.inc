<?php

/**
 * @file
 * Admin forms.
 */

/**
 * Configuration form.
 */
function orpheagateway_config_form($form, &$form_state) {
  $form['orpheagateway_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Orphea Gateway configuration'),
    '#tree' => TRUE,
  );

  $config = variable_get('orpheagateway_config', array());
  $form['orpheagateway_config']['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Base URL'),
    '#default_value' => isset($config['url']) ? $config['url'] : '',
    '#required' => TRUE,
  );
  $form['orpheagateway_config']['username'] = array(
    '#type' => 'textfield',
    '#title' => t('User name'),
    '#default_value' => isset($config['username']) ? $config['username'] : '',
    '#required' => TRUE,
  );
  $form['orpheagateway_config']['password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => isset($config['password']) ? $config['password'] : '',
    '#required' => TRUE,
  );
  $content_types = node_type_get_names();
  $content_type_description = t('This content type will be used when harvesting images metadata.')
  . ' ' . t('WARNING: All required fields will be created and added to this content type.');

  $form['orpheagateway_config']['content_type'] = array(
    '#type' => 'select',
    '#title' => t('Content type'),
    '#description' => $content_type_description,
    '#options' => $content_types,
    '#default_value' => isset($config['content_type']) ? $config['content_type'] : '',
    '#empty_value' => '',
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validate configuration form.
 */
function orpheagateway_config_form_validate($form, $form_state) {
  $values = $form_state['values'];

  $url = $values['orpheagateway_config']['url'];
  $username = $values['orpheagateway_config']['username'];
  $password = $values['orpheagateway_config']['password'];

  $service = new OrpheaGatewayService($url, $username, $password);
  $unid = $service->createSession();
  if (empty($unid)) {
    $message = t("Unable to create a session. Please check that the following parameters are correct: @url, @username, @password", array(
      '@url' => t('Base URL'),
      '@username' => t('User name'),
      '@password' => t('Password'),
    ));
    form_set_error('', $message);
    form_set_error('orpheagateway_config][url');
    form_set_error('orpheagateway_config][username');
    form_set_error('orpheagateway_config][password');
  }
}

/**
 * Submit configuration form.
 */
function orpheagateway_config_form_submit($form, $form_state) {
  $values = $form_state['values'];
  $config = $values['orpheagateway_config'];

  variable_set('orpheagateway_config', $config);

  module_load_include('inc', 'orpheagateway', 'includes/orpheagateway.fields');
  orpheagateway_add_content_type_fields($config['content_type']);
}
