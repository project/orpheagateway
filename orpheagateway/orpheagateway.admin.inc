<?php

/**
 * @file
 * Admin pages.
 */

/**
 * Test page.
 */
function orpheagateway_test() {
  $output = '<h1>Orphea Gateway Test</h1>';
  $config = variable_get('orpheagateway_config');
  $service = new OrpheaGatewayService($config['url'], $config['username'], $config['password']);
  $unid = $service->createSession();

  if ($unid) {
    $output .= '<p>Configuration OK (UNID: ' . $unid . ')</p>';

    $version = $service->version();
    $output .= '<h2>System informations</h2>';
    $output .= '<dl>';
    foreach ($version as $key => $value) {
      $output .= '<dt>' . $key . '</dt>';
      $output .= '<dd>' . $value . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    $output .= '<p>Configuration KO: Failed to create a session.</p>';
  }

  $output .= "<h2>First results from search</h2>";

  $fields = $service->getObjectFields();
  $results = $service->search(NULL, array(
    'maxrows' => 5,
    'orderby' => 'id_objet',
  ));
  foreach ($results['row'] as $object) {
    foreach ($fields as $key => $field) {
      $value = isset($object[$key]) ? $object[$key] : NULL;
      if (!isset($value) && isset($object[$field['alias']])) {
        $value = $object[$field['alias']];
      }
      if (is_array($value)) {
        $value = implode(', ', $value);
      }
      $output .= "<b>" . $field['name'] . ":</b> ";
      $output .= $value;
      $output .= "<br />";
    }

    $thumbnail_url = $object['url_full'];
    $output .= "<b>Thumbnail:</b> ";
    $output .= "<a href='$thumbnail_url'>$thumbnail_url</a>";
    $output .= "<br />";

    $assets_url = $service->getAssetsURL($object['id_objet'], OrpheaGatewayFileType::ZOOM, TRUE);
    $url = $assets_url['url_full'];
    $output .= "<b>URL:</b> ";
    $output .= "<a href='$url'>$url</a>";

    $output .= "<hr />";
  }

  return $output;
}
