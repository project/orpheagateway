<?php

/**
 * @file
 * Helper functions for harvest.
 */

/**
 * Get all available objectids.
 */
function orpheagateway_build_objectids_array($service) {
  $objectids = array();

  $search_options = array(
    'extended' => 0,
    'orderby' => 'id_objet',
    'maxrows' => 1000,
  );
  $i = 0;
  do {
    $results = $service->search(NULL, array('startfrom' => 1000 * $i) + $search_options);
    $results = $results['row'];
    foreach ($results as $result) {
      $objectids[] = $result['id_objet'];
    }
    $i++;
  } while (count($results) == 1000);

  return $objectids;
}

/**
 * Get only objectids that are between $from and $to (both inclusive).
 */
function orpheagateway_extract_objectids_from_parameters($objectids, $from, $to) {
  $offset = 0;
  $length = 0;
  $i = 0;
  while (isset($objectids[$i]) && $objectids[$i] < $from) {
    $i++;
  }
  $offset = $i;
  while (isset($objectids[$i]) && $objectids[$i] <= $to) {
    $i++;
  }
  $length = $i - $offset;

  $ids = array_slice($objectids, $offset, $length);
  return $ids;
}

/**
 * Harvest a batch of objects.
 */
function orpheagateway_harvest_objects($service, $objectids = array(), $options = array()) {
  $verbose = isset($options['verbose']) ? $options['verbose'] : 0;
  $harvested_count = 0;

  if (empty($objectids)) {
    print "No objectid given.\n";
    return 0;
  }

  $failed_objectids = array();
  $chunks = array_chunk($objectids, 100);
  foreach ($chunks as $chunkid => $chunk) {
    if ($verbose) {
      print "Harvesting chunk $chunkid (from " . reset($chunk) . " to " . end($chunk) . ")\n";
    }
    foreach ($chunk as $objectid) {
      $nid = orpheagateway_harvest_object($service, $objectid, $options);
      if (isset($nid)) {
        $harvested_count++;
      }
      else {
        $failed_objectids[] = $objectid;
      }
    }
  }

  // Retry to harvest objects that have failed.
  if (!empty($failed_objectids)) {
    error_log("Some objects have failed to be retrieved. Retrying now...");
    for ($i = 0; $i < count($failed_objectids); $i++) {
      $objectid = $failed_objectids[$i];
      $nid = orpheagateway_harvest_object($service, $objectid, $options);
      if (isset($nid)) {
        $harvested_count++;
        unset($failed_objectids[$i]);
      }
    }
  }

  if ($verbose) {
    print "Total objects harvested: $harvested_count\n";
  }

  // If there is still objects that cannot be retrieved, print their ids and
  // finish.
  if (!empty($failed_objectids)) {
    error_log("Some objects have failed again to be retrieved. Their ids are: " . implode(', ', $failed_objectids));
    error_log("You can try to harvest them again with this command: drush orpheagateway-harvest --ids=" . implode(',', $failed_objectids));
  }

  return $harvested_count;
}

/**
 * Harvest one object.
 */
function orpheagateway_harvest_object($service, $objectid, $options = array()) {
  module_load_include('inc', 'orpheagateway', 'includes/orpheagateway_records');

  $verbose = isset($options['verbose']) ? $options['verbose'] : 0;

  $nid = orpheagateway_records_get_nid($objectid);
  $metadata = $service->getMetadata($objectid, 0, 0, 1);
  if ($metadata) {
    $content_type = orpheagateway_content_type();
    $object = orpheagateway_metadata_to_object($service, $metadata);
    $zoom_asset_url = $service->getAssetsURL($objectid, OrpheaGatewayFileType::ZOOM, TRUE);
    $web_thumbnail_asset_url = $service->getAssetsURL($objectid, OrpheaGatewayFileType::WEB_THUMBNAIL, TRUE);
    if ($zoom_asset_url !== FALSE && $web_thumbnail_asset_url !== FALSE) {
      $object['zoom_url'] = $zoom_asset_url['url_full'];
      $object['web_thumbnail_url'] = $web_thumbnail_asset_url['url_full'];
      if ($nid) {
        $node = node_load($nid);
        $node = orpheagateway_object_to_node($service, $content_type, $object, $node);
        node_save($node);
        if ((int) $verbose >= 2) {
          print "\tObject $objectid: node updated (nid: $nid, title: " . $node->title . ")\n";
        }
      }
      else {
        $node = orpheagateway_object_to_node($service, $content_type, $object);
        $node = node_submit($node);
        node_save($node);
        orpheagateway_records_add($node->nid, $objectid);
        if ((int) $verbose >= 2) {
          $nid = $node->nid;
          print "\tObject $objectid: node created (nid: $nid, title: " . $node->title . ")\n";
        }
      }
      return $node->nid;
    }
    else {
      error_log("An error occured while retrieving images URLs for object $objectid. Network may be down. Please retry later.");
    }
  }
  elseif ($metadata !== FALSE) {
    // Remote object doesn't exist.
    // If a corresponding node exists, we have to remove it.
    if ($nid) {
      node_delete($nid);
      if ((int) $verbose >= 2) {
        print "\tObject $objectid: node deleted (nid: $nid)\n";
      }
      return 0;
    }
  }
  else {
    // getMetadata returned FALSE. Print an error message.
    error_log("An error occured while retrieving metadata for object $objectid. Network may be down. Please retry later.");
  }

  return NULL;
}

/**
 * Retrieve object fields and store it in a static variable.
 */
function orpheagateway_get_object_fields($service) {
  $fields = &drupal_static(__FUNCTION__);

  if (!isset($fields)) {
    $fields = $service->getObjectFields();
  }

  return $fields;
}

/**
 * Transform a metadata array into an Orphea object.
 *
 * Orphea object is an associative array where keys are field ID (pk_field).
 */
function orpheagateway_metadata_to_object($service, $metadata) {
  $object = array();

  $fields = orpheagateway_get_object_fields($service);
  foreach ($fields as $pk_field => $field) {
    list($type, $name) = explode(".", $field['fieldname']);
    $names = array(drupal_strtolower($name), $field['alias']);
    $value = NULL;

    switch ($type) {
      case 'OBJETS':
        foreach ($names as $name) {
          if (isset($metadata['object'][$name])) {
            $value = $metadata['object'][$name];
            break;
          }
        }
        break;

      case 'DESCRIPTION':
      case 'IPTC':
        $key = drupal_strtolower($type);
        foreach ($names as $name) {
          if (isset($metadata[$key][$name])) {
            $value = $metadata[$key][$name];
            break;
          }
        }
        break;

      case 'IPTC_KEYWORDS':
        $value = array();
        // $metadata['keywords']['item'] can be either an array of assocative
        // arrays (when there are multiple keywords) or a single associative
        // array.
        if (isset($metadata['keywords']['item'][0])) {
          foreach ($metadata['keywords']['item'] as $item) {
            $value[] = $item['term'];
          }
        }
        elseif (isset($metadata['keywords']['item']['term'])) {
          $value[] = $metadata['keywords']['item']['term'];
        }
        break;

      case 'IPTC_CATEGORIES':
        $value = array();
        if (isset($metadata['categories']['item'])) {
          foreach ($metadata['categories']['item'] as $item) {
            $value[] = $item['term'];
          }
        }
        break;
    }

    if (isset($value)) {
      $object[$pk_field] = $value;
    }
  }

  $object['title'] = $metadata['object']['title'];

  return $object;
}

/**
 * Transform an Orphea object into Drupal node.
 */
function orpheagateway_object_to_node($service, $content_type, $object, $node = NULL) {
  if (!isset($node)) {
    // Prepare node.
    $node = new stdClass();
    $node->type = $content_type;
    node_object_prepare($node);
    $node->uid = 1;
    $node->comment = 0;
    $node->language = 'und';
  }

  $node->title = $object['title'];

  $object_fields = orpheagateway_get_object_fields($service) + array(
    'zoom_url' => array(),
    'web_thumbnail_url' => array(),
  );
  foreach ($object_fields as $name => $object_field) {
    $field_name = 'field_orphea_' . $name;
    if (isset($object[$name])) {
      if (is_array($object[$name])) {
        $node->{$field_name}['und'] = array();
        foreach ($object[$name] as $value) {
          $node->{$field_name}['und'][] = array(
            'value' => $value,
          );
        }
      }
      else {
        $node->{$field_name}['und'][0]['value'] = $object[$name];
      }
    }
  }

  return $node;
}
