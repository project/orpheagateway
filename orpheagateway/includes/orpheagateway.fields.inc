<?php

/**
 * @file
 * Helper functions for fields.
 */

/**
 * Add required fields to a content type.
 */
function orpheagateway_add_content_type_fields($content_type_name) {
  $required_fields = orpheagateway_required_fields() + array(
    'zoom_url' => array('name' => t('ZOOM URL')),
    'web_thumbnail_url' => array('name' => t('Web Thumbnail URL')),
  );
  $fields = field_info_fields();
  $instances = field_info_instances('node', $content_type_name);
  foreach ($required_fields as $name => $required_field) {
    $field = orpheagateway_field($name, $required_field);
    if (!isset($fields[$field['field_name']])) {
      field_create_field($field);
      drupal_set_message(t("Created field @field_name", array('@field_name' => $field['field_name'])));
    }
    $instance = orpheagateway_field_instance($name, $required_field, $content_type_name);
    if (!isset($instances[$instance['field_name']])) {
      field_create_instance($instance);
      drupal_set_message(t("Created instance of field @field_name", array('@field_name' => $instance['field_name'])));
    }
  }
}

/**
 * Get the list of required fields.
 */
function orpheagateway_required_fields() {
  $config = variable_get('orpheagateway_config');
  $service = new OrpheaGatewayService($config['url'], $config['username'], $config['password']);
  $object_fields = $service->getObjectFields();
  return $object_fields;
}

/**
 * Build field settings.
 */
function orpheagateway_field($name, $object_field) {
  $name = 'field_orphea_' . $name;

  return array(
    'field_name' => $name,
    'type' => 'text_long',
    // Unlimited.
    'cardinality' => -1,
  );
}

/**
 * Build a field instance.
 */
function orpheagateway_field_instance($name, $object_field, $content_type_name) {
  $name = 'field_orphea_' . $name;
  $label = $object_field['name'];

  return array(
    'label' => $label,
    'field_name' => $name,
    'entity_type' => 'node',
    'bundle' => $content_type_name,
  );
}
