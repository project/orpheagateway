<?php

/**
 * @file
 * Functions to manipulate orpheagateway_records data.
 */

/**
 * Add a new entry in orpheagateway_records.
 */
function orpheagateway_records_add($nid, $objectid) {
  db_insert('orpheagateway_records')
    ->fields(array(
      'nid' => $nid,
      'objectid' => $objectid,
    ))
    ->execute();
}

/**
 * Get nid from objectid.
 */
function orpheagateway_records_get_nid($objectid) {
  $result = db_select('orpheagateway_records', 'o')
    ->fields('o', array('nid'))
    ->condition('objectid', $objectid)
    ->execute()
    ->fetchAssoc();

  return $result ? $result['nid'] : NULL;
}

/**
 * Get objectid from nid.
 */
function orpheagateway_records_get_objectid($nid) {
  $result = db_select('orpheagateway_records', 'o')
    ->fields('o', array('objectid'))
    ->condition('nid', $nid)
    ->execute()
    ->fetchAssoc();

  return $result ? $result['objectid'] : NULL;
}

/**
 * Get all entries from orpheagateway_records.
 */
function orpheagateway_records_get() {
  $result = db_select('orpheagateway_records', 'o')
    ->fields('o')
    ->execute()
    ->fetchAll(PDO::FETCH_ASSOC);

  return $result;
}

/**
 * Remove an entry from orpheagateway_records.
 */
function orpheagateway_records_del($nid) {
  db_delete('orpheagateway_records')
    ->condition('nid', $nid)
    ->execute();
}
