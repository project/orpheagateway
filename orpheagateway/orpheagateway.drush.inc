<?php
/**
 * @file
 * drush command for harvesting and deleting biblios.
 */

/**
 * Implements hook_drush_command().
 */
function orpheagateway_drush_command() {
  $items = array();

  $items['orpheagateway-harvest'] = array(
    'description' => dt('Import a set of Orphea objects records into Drupal.'),
    'aliases'     => array('orphea-harvest'),
    'options' => array(
      'ids' => 'A list of object ids, separated by commas',
      'from' => 'First object id to harvest',
      'to' => 'Last object id to harvest',
      'no-update' => 'Do not update existing nodes ("create only" mode)',
    ),
  );
  $items['orpheagateway-update'] = array(
    'description' => dt('Update Orphea nodes.'),
    'aliases' => array('orphea-update'),
  );
  $items['orpheagateway-sync'] = array(
    'description' => dt('Synchronize remote Orphea database with local Orphea nodes.'),
    'aliases' => array('orphea-sync'),
  );

  return $items;
}

/**
 * Prepare orpheagateway harvest.
 */
function drush_orpheagateway_harvest() {
  module_load_include('inc', 'orpheagateway', "includes/orpheagateway.harvest");

  $verbose = drush_get_option('verbose');
  $ids = drush_get_option('ids');
  $from = drush_get_option('from');
  $to = drush_get_option('to');
  $no_update = drush_get_option('no-update');

  if (isset($from) xor isset($to)) {
    drush_log(t('You must use both --from and --to options'), 'error');
    return;
  }
  if (isset($ids) && isset($from)) {
    drush_log(t('You cannot use --ids and --from/--to options at the same time'), 'error');
    return;
  }

  $service = orpheagateway_service();
  $objectids = orpheagateway_build_objectids_array($service);
  if (isset($ids)) {
    $ids = explode(',', $ids);
  }
  else {
    $ids = orpheagateway_extract_objectids_from_parameters($objectids, $from, $to);
  }

  if ($no_update) {
    module_load_include('inc', 'orpheagateway', 'includes/orpheagateway_records');
    $records = orpheagateway_records_get();
    $oids = array_map(function($record) {
      return $record['objectid'];
    }, $records);
    $ids = array_diff($ids, $oids);
  }

  orpheagateway_harvest_objects($service, $ids, array('verbose' => $verbose));
}

/**
 * Update Orphea nodes.
 */
function drush_orpheagateway_update() {
  module_load_include('inc', 'orpheagateway', "includes/orpheagateway.harvest");
  module_load_include('inc', 'orpheagateway', "includes/orpheagateway_records");

  $verbose = drush_get_option('verbose');
  $service = orpheagateway_service();

  $records = orpheagateway_records_get();
  $ids = array_map(function($record) {
    return $record['objectid'];
  }, $records);

  orpheagateway_harvest_objects($service, $ids, array('verbose' => $verbose));
}

/**
 * Synchronize remote Orphea database with local Orphea nodes.
 */
function drush_orpheagateway_sync() {
  module_load_include('inc', 'orpheagateway', "includes/orpheagateway.harvest");
  module_load_include('inc', 'orpheagateway', "includes/orpheagateway_records");

  $verbose = drush_get_option('verbose');
  $service = orpheagateway_service();

  $records = orpheagateway_records_get();
  $ids = array_map(function($record) {
    return $record['objectid'];
  }, $records);

  $oids = orpheagateway_build_objectids_array($service);
  $ids = array_unique(array_merge($ids, $oids));
  sort($ids);

  orpheagateway_harvest_objects($service, $ids, array('verbose' => $verbose));
}
