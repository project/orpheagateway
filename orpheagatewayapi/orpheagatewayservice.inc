<?php

/**
 * @file
 * Main class for Orphea Gateway.
 */

abstract class OrpheaGatewayFileType {
  const OFFICEFX_THUMBNAIL = 1;
  const WEB_THUMBNAIL = 2;
  const ZOOM = 3;
  const ORIGINAL = 4;
}

class OrpheaGatewayService {
  protected $url;
  protected $username;
  protected $password;

  protected $unid;

  /**
   * Constructor.
   */
  public function __construct($url, $username, $password) {
    $this->url = $url;
    $this->username = $username;
    $this->password = $password;
  }

  /**
   * Create session.
   */
  public function createSession($language = 1) {
    $params = array(
      'function' => 'f_createsession',
      'username' => $this->username,
      'password' => $this->password,
      'language' => $language,
    );
    $response = $this->requestService($params);
    if ($response && $response->headers['content-type'] == 'text/xml') {
      $xml = @simplexml_load_string($response->data);
      $this->unid = (string) $xml->row->unid;
    }

    return $this->unid;
  }

  /**
   * Call an Orphea function.
   *
   * Do not call this method directly.
   */
  public function call($function, $params = array()) {
    $response = NULL;

    if (!isset($this->unid)) {
      $this->createSession();
    }
    if (isset($this->unid)) {
      $params += array(
        'unid' => $this->unid,
        'function' => $function,
      );
      $response = $this->requestService($params);
      if (isset($response)) {
        $xml = simplexml_load_string($response->data);
        $response = $this->xmlToArray($xml);
      }
    }

    return $response;
  }

  /**
   * Function f_getassetsurl.
   */
  public function getAssetsURL($objectident, $filetype, $return_full_url = FALSE) {
    $params = array(
      'objectident' => $objectident,
      'filetype' => $filetype,
      'return_full_url' => $return_full_url ? 1 : 0,
    );
    $response = $this->call('f_getassetsurl', $params);
    if (isset($response)) {
      $response = isset($response['row']) ? $response['row'] : NULL;
    }
    else {
      $response = FALSE;
    }

    return $response;
  }

  /**
   * Function f_getfieldparameters.
   */
  public function getFieldParameters($maxrows = NULL, $orderby = NULL, $pk_language = NULL, $kfosearch = NULL, $pk_fields = NULL, $return_all_thesaurus = NULL) {
    $params = array(
      'maxrows' => $maxrows,
      'orderby' => $orderby,
      'pk_language' => $pk_language,
      'kfosearch' => $kfosearch,
      'pk_fields' => $pk_fields,
      'return_all_thesaurus' => $return_all_thesaurus,
    );
    $response = $this->call('f_getfieldparameters', $params);
    return $response ? $response['row'] : $response;
  }

  /**
   * Return available fields for objects.
   */
  public function getObjectFields() {
    $fields = $this->getFieldParameters();
    $object_fields = array();
    foreach ($fields as $field) {
      $object_fields[$field['pk_field']] = $field;
    }
    return $object_fields;
  }

  /**
   * Function f_getinfos.
   */
  public function getInfos($objectident) {
    $params = array(
      'objectident' => $objectident,
    );
    return $this->call('f_getinfos', $params);
  }

  /**
   * Function f_getmetadata.
   *
   * @return mixed
   *   The metadata object as an associative array if object is found.
   *   NULL if object is not found.
   *   FALSE if an error occured.
   */
  public function getMetadata($objectident, $language_id = 0, $return_blob_text = 0, $keywords_full_mode = 0, $keywords_language_filter = 0) {
    $params = array(
      'objectident' => $objectident,
      'language_id' => $language_id,
      'return_blob_text' => $return_blob_text,
      'keywords_full_mode' => $keywords_full_mode,
      'keywords_language_filter' => $keywords_language_filter,
    );
    $response = $this->call('f_getmetadata', $params);
    if (isset($response)) {
      $response = isset($response['row']) ? $response['row'] : NULL;
    }
    else {
      $response = FALSE;
    }

    return $response;
  }

  /**
   * Function f_search.
   *
   * @param string $search
   *   Query string. Can include AND / OR / NOT operators and parenthesis.
   *
   * @param array $options
   *   Associative array of options. Allowed options are:
   *   - maxrows
   *   - startfrom
   *   - extended
   *   - option (can be a string or an array of string)
   *   - orderby
   *   - fk_classtreenode
   *   - use_column_aliases
   *   - export
   *   - export_format
   *   - export_behavior
   *   - return_iptc
   *   - check_attached_file
   *   - return_keywords
   *   - return_categories
   *   - keywords_language_filter
   *   - keywords_count
   *   - keywords_maxrows
   *   - return_signatures
   *   - signatures_maxrows
   *
   *   See the official documentation for more informations on those options.
   */
  public function search($search = NULL, $options = array()) {
    if (isset($options['option']) && is_array($options['option'])) {
      $options['option'] = implode('|', $options['option']);
    }
    $params = array(
      'search' => $search,
    ) + $options;
    return $this->call('f_search', $params);
  }

  /**
   * Function f_version.
   */
  public function version() {
    $response = $this->call('f_version');
    return $response ? $response['row'] : NULL;
  }

  /**
   * Logout.
   */
  public function logout() {
    $this->call('f_logout');
    $this->unid = NULL;
  }

  /**
   * Make HTTP request.
   */
  protected function requestService($params = array()) {
    $url = $this->url . '?' . http_build_query($params);
    $response = drupal_http_request($url);
    if (isset($response->error)) {
      drupal_set_message(t("Error fetching URL '@url': @error", array('@url' => $url, '@error' => $response->error)), 'error');
      $response = NULL;
    }
    return $response;
  }

  /**
   * Convert SimpleXMLElement into array.
   */
  protected function xmlToArray($xml) {
    return json_decode(json_encode($xml), TRUE);
  }
}
